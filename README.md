# Development setup
- Install Node.js.
- Clone the repository.
- Install these packages via:
  - `npm install --save smooch-bot lodash`

# Understanding the scripts
1. `scripts.js` defines how the bot behaves.
1. `scripts.json` defines the keywords and its responds the bot will respond to.

# Testing and debugging
To test your scripts and the behavior of the bot, it is best to start from console.

## Console debugging
1. Goto `console` folder and run: `node index.js`
1. Type something or `hello` to get the bot to respond.

