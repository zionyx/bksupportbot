'use strict';

const _ = require('lodash');
const Script = require('smooch-bot').Script;

const scriptRules = require('./script.json');

module.exports = new Script({
    processing: {
        //prompt: (bot) => bot.say('Beep boop...'),
        receive: () => 'processing'
    },

    start: {
        receive: (bot) => {
            return bot.say('Hello there, I’m your Support Bot! How may I help? Just say HELLO to get started.')
                .then(() => 'askName');
        }
    },

    askName: {
        prompt: (bot) => bot.say('What\'s your name?' ),
        receive: (bot, message) => {
            const name = message.text;
            return bot.setProp('name', name)
            .then(() => bot.say(`Excellent. I'll call you ${name}
            Is that alright? %[Yes](postback:yes) %[No](postback:no)`))
            .then(() => 'askUsername');
        }
    },

    askUsername: {
        prompt: (bot) => {
            return bot.getProp('name')
            .then((name) => bot.say('${name}, I may need to get back to you after our conversation. May I have your email address please?' ))
        },
        receive: (bot, message) => {
            const name = bot.getProp('name');
            const email = message.text;
            return bot.setProp('email', email)
            .then(() => bot.say(`Great! If need be, I will contact you via ${email}.
            Is that fine with you, ${name}? %[Yes](postback:yes) %[No](postback:no)`))
            .then(() => bot.say('Alright. Thank you.'))
            .then(() => 'whatsTheMatter');
        }
    },

    whatsTheMatter: {
        prompt: (bot) => {
            return bot.getProp('name')
            .then((name) => bot.say('${name}, how may I help?'))
        },
        receive: (bot, message) => {
            const problem = message.text;
            return bot.setProp('problem', problem)
            .then(() => bot.say('Hmm... This seems like a tough problem. Let me connect you to the real guys. ;-) Hang on please.'))
            .then(() => 'speak');
        }
    },

    speak: {
        receive: (bot, message) => {

            let upperText = message.text.trim().toUpperCase();

            function updateSilent() {
                switch (upperText) {
                    case "CONNECT ME":
                        return bot.setProp("silent", true);
                    case "DISCONNECT":
                        return bot.setProp("silent", false);
                    default:
                        return Promise.resolve();
                }
            }

            function getSilent() {
                return bot.getProp("silent");
            }

            function processMessage(isSilent) {
                if (isSilent) {
                    return Promise.resolve("speak");
                }

                if (!_.has(scriptRules, upperText)) {
                    return bot.say(`I didn't understand that.`).then(() => 'speak');
                }

                var response = scriptRules[upperText];
                var lines = response.split('\n');

                var p = Promise.resolve();
                _.each(lines, function(line) {
                    line = line.trim();
                    p = p.then(function() {
                        console.log(line);
                        return bot.say(line);
                    });
                })

                return p.then(() => 'speak');
            }

            return updateSilent()
                .then(getSilent)
                .then(processMessage);
        }
    }
});